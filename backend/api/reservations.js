const express = require('express');

const router = express.Router();

const reservationService = require('../service/reservation.service');

router.get(`/all`, reservationService.getAllReservations);
router.get(`/find`, reservationService.findByReservationName);
router.post(`/create`, reservationService.createReservation);

module.exports = router;