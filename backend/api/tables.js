const express = require('express');

const router = express.Router();

const tableService = require('../service/table.service');

router.get(`/all`, tableService.getAllTables);
router.get(`/find`, tableService.findByNumber);
router.post(`/create`, tableService.createTable);

module.exports = router;