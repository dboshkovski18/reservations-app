const Sequelize = require('sequelize');

const sequelize = require('../db/database');

const Table = sequelize.define('table', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        tableNumber: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        capacity: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    },
    {
        timestamps: false
    });

module.exports = Table;