const Sequelize = require('sequelize');
const sequelize = require('../db/database');
const Reservation = require("../domain/reservation");
const Table = require("../domain/table");

const ReservationTable = sequelize.define('reservation_table', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        }
    },
    {
        timestamps: false
    });

Reservation.belongsToMany(Table, {through: ReservationTable});
Table.belongsToMany(Reservation, {through: ReservationTable});
ReservationTable.belongsTo(Reservation);
ReservationTable.belongsTo(Table);


module.exports = ReservationTable;