const Sequelize = require('sequelize');

const sequelize = require('../db/database');

const Reservation = sequelize.define('reservation', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        numberOfGuests: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        isSmokingArea: {
            type: Sequelize.BOOLEAN,
            allowNull: true
        },
        reservationName: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },
    {
        timestamps: false
    });

module.exports = Reservation;