'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        return await queryInterface.bulkInsert('reservations',
            [
                {numberOfGuests: 4, date: '2024-04-01T12:00:00', isSmokingArea: true, reservationName: 'Damjan'},
                {numberOfGuests: 2, date: '2024-04-04T11:00:00', isSmokingArea: false, reservationName: 'Bojan'},
                {numberOfGuests: 8, date: '2024-04-10T21:00:00', isSmokingArea: false, reservationName: 'Borche'},
            ]
        )
    },

    async down(queryInterface, Sequelize) {
        return queryInterface.bulkDelete('reservations', null, {});
    }
};
