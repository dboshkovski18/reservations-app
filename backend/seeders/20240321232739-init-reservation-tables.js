'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return await queryInterface.bulkInsert('reservation_tables',
        [
          {reservationId: 1, tableId: 1},
          {reservationId: 2, tableId: 2},
          {reservationId: 3, tableId: 1},
          {reservationId: 3, tableId: 2},
        ]
    )
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete('reservation_tables', null, {});
  }
};
