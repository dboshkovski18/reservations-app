'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        return await queryInterface.bulkInsert('tables',
            [
                {tableNumber: 1, capacity: 5},
                {tableNumber: 3, capacity: 4},
                {tableNumber: 4, capacity: 4},
                {tableNumber: 5, capacity: 4},
            ]
        )
    },

    async down(queryInterface, Sequelize) {
        return queryInterface.bulkDelete('reservations', null, {});
    }
};
