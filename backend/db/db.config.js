module.exports = {
    HOST: `localhost`,
    USER: `developer`,
    PASSWORD: `123`,
    DB: `reservations`,
    dialect: "postgres",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};