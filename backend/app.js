const express = require('express')
const cors = require('cors');
const helmet = require('helmet')
const bodyParser = require('body-parser');
const sequelize = require('./db/database')

const tablesApi = require('./api/tables')
const reservationsApi = require('./api/reservations')

const app = express()
app.use(helmet())
app.use(bodyParser.json())
app.use(cors());


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
})

app.use('/api/tables', tablesApi)
app.use('/api/reservations', reservationsApi)


sequelize.sync()
    .then(() => {
        const PORT = process.env.PORT || 8080;
        app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}.`);
        });
    })
    .catch(err => console.log(err))