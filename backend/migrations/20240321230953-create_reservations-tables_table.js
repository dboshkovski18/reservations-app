'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable('reservation_tables', {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
          },
          reservationId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'reservations',
              key: 'id'
            }
          },
          tableId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            references: {
              model: 'tables',
              key: 'id'
            }
          }
  })
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable('reservation_tables')
  }
};
