const Table = require("../domain/table");
exports.createTable = async (req, res, next) => {
    try {
        const table = req.body;

        const isPresent = await Table.findOne({where: table})

        if (isPresent) {
            const error = new Error(`${isPresent.value} is exist.`);
            error.statusCode = 422;
            throw error;
        }

        console.log("Creating table: " , table)
        const savedTable = await Table.build(
            table
        ).save();


        res.status(201).json(savedTable);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.findByNumber = async (req, res, next) => {
    try {
        const {tableNumber} = req.query;
        const table = await Table.findOne({where: {tableNumber}})

        if (!table) {
            return res.status(404).json({message: 'Table not found'});
        }

        res.status(201).json(table)

    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}

exports.getAllTables = async (req, res, next) => {
    try {
        const tables = await Table.findAll();

        if (!tables) {
            return res.status(404).json({message: 'No tables to show'});
        }

        res.status(201).json(tables);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};