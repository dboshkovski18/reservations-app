const Reservation = require('../domain/reservation')
const tableService = require('../service/table.service')
const Table = require("../domain/table");
const reservationTableService = require("../service/reservation-table.service")

exports.createReservation = async (req, res, next) => {
    try {

        const { reservation , table} = req.body;

        console.log("Reservation info: ", reservation)
        console.log("Table to reserve: ", table)

        const isReservation = await Reservation.findOne({where: reservation})

        if (isReservation) {
            const error = new Error(`${isReservation.value} is exist.`);
            error.statusCode = 422;
            throw error;
        }

        const resultReservation = await Reservation.build(reservation).save();

        reservationTableService.createReservationTable(resultReservation,table)
            .then(result => {
                console.log('Reservation : ', result)
                res.status(201).json(result)
            })

    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}

exports.findByReservationName = async (req, res, next) => {
    try {
        const {reservationName} = req.query;

        const reservation = await Reservation.findOne({where : {reservationName : reservationName}})

        reservationTableService.findByReservation(reservation)
            .then(reservationTable => {
                console.log(reservationTable)
                res.status(201).json(reservationTable)
            })


    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}

exports.getAllReservations = async (req, res, next) => {
    try {
        // const reservations = await Reservation.findAll();

        reservationTableService.getAllReservationTables()
            .then(reservations => {
                console.log(reservations)

                if (!reservations.length) {
                    return res.status(404).json({message: 'No reservations to show'});
                }

                res.status(201).json(reservations);
            })



    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};