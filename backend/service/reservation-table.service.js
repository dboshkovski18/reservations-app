const Table = require("../domain/table");
const Reservation = require("../domain/reservation");
const ReservationTable = require("../domain/reservation-table");


exports.createReservationTable = async (reservation, table) => {
    try {
        const isPresent = await ReservationTable.findOne({
            where: {
                reservationId: reservation.id,
                tableId: table.id
            }
        });

        if (isPresent) {
            const error = new Error(`${isPresent.value} is already present.`);
            error.statusCode = 422;
            throw error;
        }


        return await ReservationTable.build({
            reservationId: reservation.id,
            tableId: table.id
        }).save();

    } catch (err) {
        console.log(err);
        throw err;
    }
};

exports.findByReservation = async (reservation) => {
    try {
        const reservationTable = await ReservationTable.findAll({
            where: {reservationId: reservation.id},
            include: [Reservation, Table]
        })

        if (!reservationTable) {
            return res.status(404).json({message: 'Reservation-table not found'});
        }

        return reservationTable
    } catch (err) {
        console.log(err)
        throw err
    }
}

exports.getAllReservationTables = async () => {
    try {
        return await ReservationTable.findAll({
            include: [Reservation, Table]
        })
    } catch (err) {
        console.log(err);
        throw err;
    }
};