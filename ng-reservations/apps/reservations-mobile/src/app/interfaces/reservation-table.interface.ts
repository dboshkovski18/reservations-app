export interface ReservationTable {
  "id": number,
  "reservationId": number,
  "tableId": number,
  "reservation": {
    "id": number,
    "numberOfGuests": number,
    "date": string,
    "isSmokingArea": boolean,
    "reservationName": string
  },
  "table": {
    "id": number,
    "tableNumber": number,
    "capacity": number
  }
}



