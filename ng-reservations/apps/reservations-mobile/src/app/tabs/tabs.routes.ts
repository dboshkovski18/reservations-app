import { Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

export const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'reservations',
        loadComponent: () =>
          import('../pages/reservations/reservations.page').then((m) => m.ReservationsPage),
      },
      {
        path: 'tables',
        loadComponent: () =>
          import('../pages/tables/tables.page').then((m) => m.TablesPage),
      },
      {
        path: '',
        redirectTo: '/tabs/reservations',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/tabs/reservations',
    pathMatch: 'full',
  },
];
