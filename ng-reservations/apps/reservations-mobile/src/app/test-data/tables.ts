export const tables = [
  {
    tableNumber : 1,
    capacity : 5,
    isSmoking : false
  },
  {
    tableNumber : 3,
    capacity : 4,
    isSmoking : false
  },
  {
    tableNumber : 5,
    capacity : 4,
    isSmoking : false
  },
  {
    tableNumber : 7,
    capacity : 2,
    isSmoking : true
  },
  {
    tableNumber : 9,
    capacity : 2,
    isSmoking : true
  },
  {
    tableNumber : 15,
    capacity : 5,
    isSmoking : false
  },
  {
    tableNumber : 17,
    capacity : 4,
    isSmoking : false
  },
  {
    tableNumber : 13,
    capacity : 2,
    isSmoking : true
  },
]
