export const reservations = [
  {
    reservationName : 'Дамјан',
    numberOfGuests : 4,
    date : '01-03-2024',
    isSmokingArea : true
  },
  {
    reservationName : 'Стојан',
    numberOfGuests : 6,
    date : '01-04-2024',
    isSmokingArea : false
  },
  {
    reservationName : 'Мила',
    numberOfGuests : 2,
    date : '05-12-2024',
    isSmokingArea : true
  },
  {
    reservationName : 'Петар',
    numberOfGuests : 3,
    date : '04-06-2024',
    isSmokingArea : false
  },
  {
    reservationName : 'Хилда',
    numberOfGuests : 8,
    date : '06-07-2024',
    isSmokingArea : true
  },
  {
    reservationName : 'Ивана',
    numberOfGuests : 2,
    date : '01-03-2024',
    isSmokingArea : false
  },{
    reservationName : 'Дарко',
    numberOfGuests : 4,
    date : '01-03-2024',
    isSmokingArea : true
  },
  {
    reservationName : 'Марјан',
    numberOfGuests : 6,
    date : '01-04-2024',
    isSmokingArea : true
  },
  {
    reservationName : 'Стојко',
    numberOfGuests : 2,
    date : '05-12-2024',
    isSmokingArea : true
  },
  {
    reservationName : 'Филип',
    numberOfGuests : 3,
    date : '04-06-2024',
    isSmokingArea : true
  },
  {
    reservationName : 'Дарија',
    numberOfGuests : 8,
    date : '06-07-2024',
    isSmokingArea : true
  },
  {
    reservationName : 'Тони',
    numberOfGuests : 2,
    date : '01-03-2024',
    isSmokingArea : false
  },

]
