import {LoadingController, ToastController} from "@ionic/angular/standalone";
import {Injectable} from "@angular/core";
import {load} from "@angular-devkit/build-angular/src/utils/server-rendering/esm-in-memory-loader/loader-hooks";

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(private _loadingController: LoadingController) {}

  async startLoading() {
    const loading =await this._loadingController.create({
      message: 'Се вчитува...',
    });
    return loading.present()
  }

  async dismissLoading() {
    await this._loadingController.dismiss();
  }


}
