import {ToastController} from "@ionic/angular/standalone";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  constructor(private _toastController: ToastController) {
  }



  async successNotification(message : string) {
    const toast = await this._toastController.create({
      message: message,
      duration: 1500,
      position: 'bottom',
      positionAnchor : 'ion-tab-bar',
      cssClass: 'success-toast'
    });

    await toast.present();
  }

  async errorNotification(message : string) {
    const toast = await this._toastController.create({
      message: message,
      duration: 1500,
      position: 'bottom',
      positionAnchor : 'ion-tab-bar',
      cssClass : 'error-toast'
    });

    await toast.present();
  }
}
