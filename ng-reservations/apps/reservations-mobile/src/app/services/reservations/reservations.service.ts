import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Server} from "../../constants/server.constants";
import {ReservationTable} from "../../interfaces/reservation-table.interface";
import {reservations} from "../../test-data/reservations";

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {

  constructor(private _http: HttpClient) {
  }

  findAllReservations(): Observable<ReservationTable[]> {
    return this._http.get<ReservationTable  []>(`${Server.API_URL}/reservations/all`)
  }

  createReservation(reservation: any): Observable<any> {
    return this._http.post<any>(`${Server.API_URL}/reservations/create`, reservation)
  }
}
