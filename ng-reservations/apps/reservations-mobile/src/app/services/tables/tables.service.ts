import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Server} from "../../constants/server.constants";
import {Table} from "../../interfaces/table.interface";

@Injectable({
  providedIn: 'root'
})
export class TablesService {

  constructor(private _http : HttpClient) { }

  readonly path = '/api/tables'

  findAllTables() : Observable<any> {
    return this._http.get(`${Server.API_URL}/tables/all`)
  }
  findTableByNumber(tableNumber: number) : Observable<any> {
    return this._http.get(`${Server.API_URL}/tables/find?tableNumber=${tableNumber}`);
  }

  addTable(table : Table) : Observable<any> {
    return this._http.post(`${Server.API_URL}/tables/create`, table)
  }

}
