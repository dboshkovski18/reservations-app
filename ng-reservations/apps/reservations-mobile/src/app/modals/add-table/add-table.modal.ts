import { Component, OnInit } from '@angular/core';
import {
  IonButton,
  IonButtons, IonCol, IonContent,
  IonHeader,
  IonInput, IonItem, IonLabel, IonList, IonRow,
  IonTitle,
  IonToolbar,
  ModalController
} from "@ionic/angular/standalone";
import {ReactiveFormsModule, UntypedFormBuilder, UntypedFormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-table.modal',
  templateUrl: './add-table.modal.html',
  styleUrls: ['./add-table.modal.scss'],
  imports: [
    IonHeader,
    IonToolbar,
    IonButtons,
    IonButton,
    IonTitle,
    IonInput,
    IonItem,
    IonContent,
    IonList,
    ReactiveFormsModule,
    IonCol,
    IonLabel,
    IonRow
  ],
  standalone: true
})
export class AddTableModal implements OnInit {

  addTableForm: UntypedFormGroup = this._formDefinition;


  constructor(private _modalCtrl: ModalController, private _formBuilder : UntypedFormBuilder) { }

  private get _formDefinition() {
    return this._formBuilder.group({
      tableNumber: ['', Validators.required],
      capacity: ['', Validators.required]
    });
  }


  ngOnInit() {}

  dismissModal(){
    this._modalCtrl.dismiss({submitted: false})
  }

  submitData(){
    this._modalCtrl.dismiss({submitted: true, table : this.addTableForm.value})
  }

}
