import {Component, OnInit} from '@angular/core';
import {
  IonButton,
  IonButtons, IonCheckbox, IonCol, IonContent, IonDatetime, IonDatetimeButton,
  IonHeader,
  IonInput, IonItem, IonLabel, IonList, IonModal, IonRow, IonSelect, IonSelectOption,
  IonTitle,
  IonToolbar,
  ModalController
} from "@ionic/angular/standalone";
import {ReactiveFormsModule, UntypedFormBuilder, UntypedFormGroup, Validators} from "@angular/forms";
import {TablesService} from "../../services/tables/tables.service";
import {LoadingService} from "../../services/loading/loading.service";
import {catchError, map, of, Subscription} from "rxjs";
import {NotificationService} from "../../services/notifications/notification.service";
import {NgForOf} from "@angular/common";

@Component({
  selector: 'app-add-reservation',
  templateUrl: './add-reservation.modal.html',
  styleUrls: ['./add-reservation.modal.scss'],
  standalone: true,
  imports: [
    IonHeader,
    IonToolbar,
    IonButtons,
    IonButton,
    IonTitle,
    IonInput,
    IonItem,
    IonContent,
    IonList,
    ReactiveFormsModule,
    IonCol,
    IonLabel,
    IonRow,
    IonCheckbox,
    IonDatetime,
    IonSelect,
    IonSelectOption,
    NgForOf
  ]
})
export class AddReservationModal implements OnInit {

  addReservationForm: UntypedFormGroup = this._formDefinition;
  tables?: any[];

  tablesSubscribtion$?: Subscription;

  constructor(private _modalCtrl: ModalController,
              private _formBuilder: UntypedFormBuilder,
              private _tableService: TablesService,
              private _loadingService: LoadingService,
              private _notificationService: NotificationService) {
  }


  private get _formDefinition() {
    return this._formBuilder.group({
      reservationName: ['', Validators.required],
      numberOfGuests: ['', Validators.required],
      table: ['', Validators.required],
      date: ['', Validators.required],
      isSmokingArea: [false, Validators.required],
    });
  }


  ngOnInit() {

    this._loadingService.startLoading().then(() => {
      this.tablesSubscribtion$ = this._tableService.findAllTables().pipe(
        map(data => {
          this.tables = data
          this._loadingService.dismissLoading()
        }),
        catchError(err => {
          this._loadingService.dismissLoading()
          this._notificationService.errorNotification(err.message)
          return of(err)
        })
      ).subscribe()
    })


    console.log(this.addReservationForm.value)

    this.addReservationForm.valueChanges.subscribe(_ => {
      console.log(this.addReservationForm.value)
    })
  }

  dismissModal() {
    this._modalCtrl.dismiss({submitted: false})
  }

  submitData() {
    this._loadingService.startLoading().then(() => {
      this.tablesSubscribtion$ = this._tableService.findTableByNumber(this.addReservationForm.value.table).pipe(
        map(data => {
          this.addReservationForm.value.table = data
          this._loadingService.dismissLoading()
          this._modalCtrl.dismiss({
            submitted: true,
            reservation: {
              "reservation": {
                "numberOfGuests": this.addReservationForm.value.numberOfGuests,
                "date": this.addReservationForm.value.date,
                "isSmokingArea": this.addReservationForm.value.isSmokingArea,
                "reservationName": this.addReservationForm.value.reservationName
              },
              "table": this.addReservationForm.value.table
            }
          })
        }),
        catchError(err => {
          this._loadingService.dismissLoading()
          this._notificationService.errorNotification(err.message)
          return of(err)
        })
      ).subscribe()
    })

  }

  ngOnWillLeave() {
    this.tablesSubscribtion$ && this.tablesSubscribtion$.unsubscribe()
  }

}
