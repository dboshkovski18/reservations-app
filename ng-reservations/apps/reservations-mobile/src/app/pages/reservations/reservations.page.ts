import {Component, OnInit} from '@angular/core';
import {
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonList,
  IonItem,
  IonButtons,
  IonButton, IonIcon, IonLabel, IonSelect, IonSelectOption, ModalController
} from '@ionic/angular/standalone';
import {reservations} from "../../test-data/reservations";
import {DatePipe, NgForOf, NgIf} from "@angular/common";
import { addIcons } from 'ionicons';
import {logoNoSmoking} from "ionicons/icons";
import {ReservationsService} from "../../services/reservations/reservations.service";
import {catchError, map, of, Subscription, switchMap} from "rxjs";
import {NotificationService} from "../../services/notifications/notification.service";
import {ReservationTable} from "../../interfaces/reservation-table.interface";
import {LoadingService} from "../../services/loading/loading.service";
import {AddTableModal} from "../../modals/add-table/add-table.modal";
import {AddReservationModal} from "../../modals/add-reservation/add-reservation.modal";


@Component({
  selector: 'reservations',
  templateUrl: 'reservations.page.html',
  styleUrls: ['reservations.page.scss'],
  standalone: true,
  imports: [IonHeader, IonToolbar, IonTitle, IonContent, IonList, IonItem, NgForOf, IonButtons, IonButton, IonIcon, IonLabel, IonSelect, IonSelectOption, NgIf, DatePipe],
})
export class ReservationsPage implements OnInit {

  reservations? : ReservationTable[]

  reservationsSubscription$? : Subscription
  constructor(private _reservationService : ReservationsService, private _notificationService : NotificationService,
              private _loadingService : LoadingService, private _modalCtrl : ModalController) {
    addIcons({logoNoSmoking})
  }

  ngOnInit(): void {
    this._loadingService.startLoading().then(() => {
      this.reservationsSubscription$ = this._reservationService.findAllReservations().
      pipe(
        map(data => {
          this.reservations = data;
          this._loadingService.dismissLoading();
        }),
        catchError(err => {
          this._loadingService.dismissLoading()
          this._notificationService.errorNotification(err.message)
          return of(err)
        })
      ).subscribe()
    })

  }

  async openAddReservationModal() {
    const modal = await this._modalCtrl.create({
      component: AddReservationModal,
    });

    modal.present();

    await modal.onWillDismiss().then(data => {
      const {submitted, reservation} = data.data
      if(submitted){
        this._loadingService.startLoading().then(() => {
          this._reservationService.createReservation(reservation)
            .pipe(
              switchMap(table =>
                this._reservationService.findAllReservations()
                  .pipe(
                    map(reservations => {
                      this.reservations = reservations;
                      this._loadingService.dismissLoading()
                      this._notificationService.successNotification('Успешно додадена резервација!')
                    })
                  )
              ),
              catchError(err => {
                this._loadingService.dismissLoading()
                this._notificationService.errorNotification(err.message)
                return of(err)
              })
            )
            .subscribe(next => {
            })
        })
      }
    });

  }

  ngOnWillLeave(){
    this.reservationsSubscription$ && this.reservationsSubscription$.unsubscribe()
  }

}
