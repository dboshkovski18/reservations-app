import {Component, OnInit} from '@angular/core';
import {
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonItem,
  IonList,
  IonButton,
  IonButtons, IonIcon, IonLabel, IonSelect, IonSelectOption, ToastController, ModalController
} from '@ionic/angular/standalone';
import {TablesService} from "../../services/tables/tables.service";
import {NgForOf, NgIf} from "@angular/common";
import {addIcons} from "ionicons";
import {logoNoSmoking} from "ionicons/icons";
import {catchError, isEmpty, map, of, Subscription, switchMap, tap} from "rxjs";
import {NotificationService} from "../../services/notifications/notification.service";
import {LoadingService} from "../../services/loading/loading.service";
import {AddTableModal} from "../../modals/add-table/add-table.modal";

@Component({
  selector: 'tables',
  templateUrl: 'tables.page.html',
  styleUrls: ['tables.page.scss'],
  standalone: true,
  imports: [IonHeader, IonToolbar, IonTitle, IonContent, IonItem, IonList, NgForOf, IonButton, IonButtons, IonIcon, IonLabel, IonSelect, IonSelectOption, NgIf]
})
export class TablesPage implements OnInit{

  tables? : any[];

  tablesSubscribtion$? : Subscription;

  constructor(private _tablesService : TablesService,
              private _notificationService : NotificationService,
              private _loadingService : LoadingService,
              private _modalCtrl: ModalController) {
    addIcons({logoNoSmoking})
  }


  ngOnInit(): void {
    this._loadingService.startLoading().then(() => {
     this.tablesSubscribtion$ =  this._tablesService.findAllTables().
      pipe(
        map(data => {
          this.tables = data
          this._loadingService.dismissLoading()
        }),
        catchError(err => {
          this._loadingService.dismissLoading()
          this._notificationService.errorNotification(err.message)
          return of(err)
        })
      ).subscribe()
    })

    }

  async openAddTableModal() {
      const modal = await this._modalCtrl.create({
        component: AddTableModal,
      });

      modal.present();

     await modal.onWillDismiss().then(data => {
       const {submitted, table} = data.data
      if(submitted){
        console.log('submitted')
        this._loadingService.startLoading().then(() => {
          this._tablesService.addTable(table)
            .pipe(
              switchMap(table =>
                this._tablesService.findAllTables()
                  .pipe(
                    map(tables => {
                      this.tables = tables;
                      this._loadingService.dismissLoading()
                      this._notificationService.successNotification('Успешно додадена маса!')
                    })
                  )
              ),
              catchError(err => {
                this._loadingService.dismissLoading()
                this._notificationService.errorNotification(err.message)
                return of(err)
              })
            )
            .subscribe(next => {
            })
        })
      }
    });
  }

    ngOnWillLeave(){
    this.tablesSubscribtion$ && this.tablesSubscribtion$.unsubscribe()
    }

}
