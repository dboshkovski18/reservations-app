#!/bin/bash

# Start LocalTunnel and redirect output to a temporary file
lt --port 8080 > localtunnel_output.txt &

# Wait for a moment to ensure LocalTunnel has started
sleep 5

# Read the URL from the temporary file
localtunnel_url=$(grep -o 'https://.*' localtunnel_output.txt)

# Echo the captured URL
echo "LocalTunnel URL: $localtunnel_url"

# Replace the URL in the TypeScript file
sed -i '' "s|public static API_URL = '.*'|public static API_URL = '$localtunnel_url/api'|" src/app/constants/server.constants.ts

echo "URL replaced successfully"
